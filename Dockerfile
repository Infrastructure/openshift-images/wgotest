FROM docker.io/library/wordpress:fpm

RUN apt-get update && apt-get install -y nginx-light libnginx-mod-http-geoip geoip-database rsync && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx.conf /etc/nginx/nginx.conf

RUN apt-get update && apt-get install -y libldap-common libldap-dev && \
    docker-php-ext-configure ldap && \
    docker-php-ext-install -j$(nproc) ldap
ADD ipa-ca.crt /etc/ipa-ca.crt
ADD ldap.conf /etc/ldap/ldap.conf

RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp

RUN echo no | pecl install apcu && \
    docker-php-ext-enable apcu && \
    echo 'apc.enable=1' > /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini

RUN yes no | pecl install redis && \
    docker-php-ext-enable redis

ADD php.ini /usr/local/etc/php/conf.d/wordpress.ini
RUN sed -i 's/pm.max_children =.*/pm.max_children = 10/' /usr/local/etc/php-fpm.d/www.conf

ADD entrypoint /entrypoint
ENTRYPOINT ["/entrypoint"]
